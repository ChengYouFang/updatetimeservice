﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UpdateTimeService
{
    public partial class Service1 : ServiceBase
    {
        private NTPClient ntpClient;
        private const string source = "UpdateTimeService";

        public Service1()
        {
            InitializeComponent();
            this.AutoLog = false;
            if (!EventLog.SourceExists(source))
                EventLog.CreateEventSource(source, "UpdateTimeLog");

            eventLog1.Source = source;
            ntpClient = new NTPClient();
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Start");

            bool isSet = ntpClient.GetNtpTime();
            if (isSet)
            {
                eventLog1.WriteEntry("true");
                this.OnPause();
            }
            else
                eventLog1.WriteEntry("false");

            Thread.Sleep(1000);
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("Stop");
            ntpClient = null;
        }

        private class NTPClient
        {
            protected internal bool GetNtpTime()
            {
                const string ntpServer = "time.nist.gov";
                byte[] ntpData = new byte[48];

                //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)
                ntpData[0] = 0x1B;

                IPAddress[] addresses = Dns.GetHostEntry(ntpServer).AddressList;
                IPEndPoint ipEndPoint = new IPEndPoint(addresses[0], 123);
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);
                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();


                const byte serverReplyTime = 40;
                //Get the seconds part
                ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

                //Get the seconds fraction
                ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

                //Convert From big-endian to little-endian
                intPart = SwapEndianness(intPart);
                fractPart = SwapEndianness(fractPart);

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

                //UTC time + 8 
                DateTime networkDateTime = (new DateTime(1900, 1, 1))
                    .AddMilliseconds((long)milliseconds).AddHours(8);

                if (UpdateTime.SetTime(networkDateTime))
                    return true;
                return false;
            }

            //ref stackoverflow.com/a/3294698/162671
            static uint SwapEndianness(ulong x)
            {
                return (uint)(((x & 0x000000ff) << 24) +
                               ((x & 0x0000ff00) << 8) +
                               ((x & 0x00ff0000) >> 8) +
                               ((x & 0xff000000) >> 24));
            }
            public class UpdateTime
            {
                [DllImport("kernel32.dll")]
                private static extern bool SetLocalTime(ref TIME time);

                [StructLayout(LayoutKind.Sequential)]
                private struct TIME
                {
                    public short year;
                    public short month;
                    public short dayOfWeek;
                    public short day;
                    public short hour;
                    public short minute;
                    public short second;
                    public short milliseconds;
                }


                protected internal static bool SetTime(DateTime dt)
                {
                    TIME time;
                    time.year = (short)dt.Year;
                    time.month = (short)dt.Month;
                    time.dayOfWeek = (short)dt.DayOfWeek;
                    time.day = (short)dt.Day;
                    time.hour = (short)(dt.Hour);
                    time.minute = (short)dt.Minute;
                    time.second = (short)dt.Second;
                    time.milliseconds = (short)dt.Millisecond;
                    return SetLocalTime(ref time);
                }

            }
        }
    }
}
